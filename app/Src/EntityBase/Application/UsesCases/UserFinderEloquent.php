<?php

namespace App\Src\Users\Application\UsesCases;

use App\Src\Users\Application\Actions\UserFind;
use App\Src\Users\Infrastructure\Database\UserEloquent;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserFinderEloquent
{
    public function __construct(
        private readonly UserFind $userFind
    )
    {
    }

    public function __invoke($id): ?UserEloquent
    {
        $userEloquent = $this->userFind->__invoke($id);
        if(!$userEloquent){
            throw new HttpResponseException(response()->json([
                'message' => sprintf('User <%s> no encontrado', $id),
                'code' => 404,
            ], 422));
        }
        return  $userEloquent;
    }
}
