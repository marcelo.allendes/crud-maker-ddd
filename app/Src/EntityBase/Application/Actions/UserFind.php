<?php

namespace App\Src\Users\Application\Actions;

use App\Src\Users\Domain\Contracts\UserInterface;
use App\Src\Users\Infrastructure\Database\UserEloquent;

class UserFind
{
    public function __construct(private readonly UserInterface $userInterface)
    {
    }

    public function __invoke($id): ?UserEloquent
    {
        return $this->userInterface->find($id);
    }
}
