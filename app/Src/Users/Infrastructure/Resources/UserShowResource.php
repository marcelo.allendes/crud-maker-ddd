<?php

namespace App\Src\Users\Infrastructure\Resources;

use App\Src\Users\Domain\Entities\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserShowResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var User $user */
        $user = $this;
        return [
            'id'=>$user->getId(),
            'name'=>$user->getName(),
            'email'=>$user->getEmail(),
            'password'=>$user->getPassword(),
            'remember_token'=>$user->getRememberToken(),
        ];
    }
}
