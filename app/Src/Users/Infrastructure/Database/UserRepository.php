<?php

namespace App\Src\Users\Infrastructure\Database;

use App\Src\Users\Domain\Contracts\UserInterface;
use App\Src\Users\Domain\Entities\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository implements UserInterface
{
    public function all(): Collection
    {
        return  UserEloquent::all();
    }

    public function find(int $id): ?UserEloquent
    {
        return  UserEloquent::find($id);
    }

    public function destroy(UserEloquent $userEloquent): void
    {
        $userEloquent->deleteOrFail();
    }

    public function store(User $user): User
    {
        $userEloquent = UserMapper::toEloquentModel($user);
        $userEloquent->save();
        $user->setId($userEloquent->id);
        return $user;
    }

    public function persist(UserEloquent $userEloquent): UserEloquent
    {
        $userEloquent->save();
        return $userEloquent;
    }

    public function loadRelations(UserEloquent $userEloquent, array $relations = []): UserEloquent
    {
        foreach ($relations as $relation) {
            $userEloquent->load($relation);
        }
        return $userEloquent;
    }

}
