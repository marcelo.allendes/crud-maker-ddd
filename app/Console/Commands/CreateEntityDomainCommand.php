<?php

namespace App\Console\Commands;

use App\Console\Commands\Traits\CreateEntityDomain\ProcessDTOProperties;
use App\Console\Commands\Traits\CreateEntityDomain\ProcessResourceProperties;
use App\Console\Commands\Traits\CreateEntityDomain\ProcessSettersAndGetters;
use App\Console\Commands\Traits\CreateEntityDomain\RouteProcessor;
use App\Console\Commands\Traits\CreateEntityDomain\RegisterDomainInApp;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use App\Console\Commands\Traits\CreateEntityDomain\ProcessProperties;
use Illuminate\Support\Str;

class CreateEntityDomainCommand extends Command
{
    use ProcessProperties;
    use ProcessDTOProperties;
    use ProcessSettersAndGetters;
    use ProcessResourceProperties;
    use RouteProcessor;
    use RegisterDomainInApp;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-entity-domain';
    private string $nameEntityDomainBase = "EntityBase";
    private array $properties = [];
    private string $table = "";
    private string $entityName = "";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->table = $this->ask('Favor Indicar el nombre de la tabla:');

        if (!Schema::hasTable($this->table)) {
            $this->error("Tabla no existe");
            return;
        }
        $entityPathBase = app_path("Src/".$this->nameEntityDomainBase);
        $this->entityName = ucfirst(Str::camel($this->table));
        $entityPath = app_path("Src/{$this->entityName}");

        if(File::exists($entityPath)) {
            $this->error("La entidad de dominio ya existe en el proyecto");
            return;
        }

        if(!File::exists($entityPathBase)) {
            $this->error("La entidad de dominio Base no existe en el proyecto");
            return;
        }

        $this->copyDomain($entityPathBase, $entityPath);
        $this->setProperties();
        $this->processFiles($entityPath);
        $this->processRoutes();
        $this->procesRegisterApp();
        $this->info("La tabla es: {$this->table}");
    }

    public function setProperties(): void
    {
        $columns = Schema::getColumnListing($this->table);
        foreach ($columns as $column){
            $type = DB::getSchemaBuilder()->getColumnType($this->table, $column);
            $this->properties[] = [$column=>$type];
        }

    }

    public function copyDomain($source, $destination): void
    {
        if(File::copyDirectory($source, $destination)){
            $this->alert("Directorio creado correctamente");
        }else{
            $this->error("Directorio no creado. Fin de la ejecucion");
            exit(1);
        }

    }

    public function processFiles($path): void
    {
        foreach (File::allFiles($path) as $file) {
            $singleName = substr($this->entityName,0,-1);
            $newNameFile = str_replace("User", $singleName, $file);
            $content = File::get($file);
            if(strstr($file, "Entities/User.php")){
                $content = $this->processProperties($content);
                //vamos a procesar el dominio, metiendo sus propiedades getters and setters
                $content = $this->doProcessSettersAndGetters($content);
            }
            if(strstr($file, "DTO/UserDTO.php")){
                $content = $this->processDTOProperties($content);
                //vamos a procesar el DTO, metiendo sus propiedades getters and setters
            }

            $content = str_replace('Src\User', "Src\\$singleName", $content);//Esto es para definir el nameSpace
            $content = str_ireplace('$user', '$'.lcfirst($singleName), $content);
            $content = str_ireplace('->user', '->'.lcfirst($singleName), $content);
            $content = str_ireplace('User', $singleName, $content);
            $content = str_replace('/*FieldsDTOCreator*/', $this->getFieldsForDTO('create'), $content);//Para procesar los DTOS del create
            $content = str_replace('/*FieldsDTOUpdater*/', $this->getFieldsForDTO('update'), $content);//Para procesar los DTOS del update
            $content = str_replace('/*nameTableEloquent*/', sprintf('protected $table = "%s";', $this->table), $content);
            $content = $this->getResourceField($content);
            File::put($newNameFile, $content);
            unlink($file);
            $this->info($file);  // Mostrar el path por pantalla
        }
    }

    public function processRoutes(): void
    {
        $file = base_path('routes/api.php');
        $content = File::get($file);

        $content = $this->goProcessRoute($content);
        File::put($file,$content);
    }

    public function procesRegisterApp(): void
    {
        $file = base_path('app/Providers/AppServiceProvider.php');
        $content = File::get($file);

        $content = $this->goProcessProviderApp($content);
        File::put($file,$content);
    }
}
