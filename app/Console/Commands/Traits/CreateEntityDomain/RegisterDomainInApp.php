<?php

namespace App\Console\Commands\Traits\CreateEntityDomain;

trait RegisterDomainInApp
{
    public function goProcessProviderApp(string $content): string
    {
        $singleName = substr($this->entityName,0,-1);
        $registerApp = sprintf('$this->app->bind(%sInterface::class, %sRepository::class);',
            $singleName,
            $singleName
        );

        $registerApp.="
        /*AquiNuevoDomain*/";
        $content = str_replace("/*AquiNuevoDomain*/", $registerApp, $content);

        $includeFiles = sprintf("use App\Src\%s\Domain\Contracts\%sInterface;
use App\Src\%s\Infrastructure\Database\%sRepository;",
        $this->entityName,
        $singleName,
        $this->entityName,
            $singleName
        );
        $includeFiles.="
/*includeInterface*/";
        $content = str_replace("/*includeInterface*/", $includeFiles, $content);
        return $content;
    }
}
