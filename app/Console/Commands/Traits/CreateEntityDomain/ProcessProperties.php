<?php

namespace App\Console\Commands\Traits\CreateEntityDomain;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

trait  ProcessProperties
{
    public function processProperties(string $content): string
    {
        //$propertyDefinitions = implode('\\n', $this->getDefinitionField());
        $propertyDefinitions = $this->getDefinitionField();
        $content = str_replace('//**PropertiesZone', $propertyDefinitions, $content);

        return $content;
    }

    public function getColumns(): array
    {
        $result = [];
        $columns = Schema::getColumnListing($this->table);
        foreach ($columns as $column) {
            $result[] = $column;
        }
        return $result;
    }

    public function getDefinitionField(): string
    {
        $result = "";
        foreach ($this->getColumns() as $column) {
            $result.= sprintf('private $%s;', Str::camel($column))."
    ";
        }
        return $result;
    }

    public function getFieldsForDTO(string $mode): string
    {
        $result = "";
        foreach ($this->getColumns() as $column) {
            if($column == 'id'){
                continue;
            }
            $result.= sprintf('$data[%s] ?? null, ',"'".$column."'");
        }
        return $result;
    }

}
